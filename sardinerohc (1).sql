-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-05-2023 a las 22:42:53
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sardinerohc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `cod_categoria` varchar(5) NOT NULL,
  `dni` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`cod_categoria`, `dni`) VALUES
('BN_M', '11111111A'),
('IF_M', '11111111A'),
('PB_M', '11111111A'),
('PB_F', '12121212J'),
('BN_F', '13131313K'),
('JUV_M', '14141414L'),
('AL_F', '16161616N'),
('AL_M', '16161616N'),
('SEG_F', '33333333C'),
('JUV_F', '55555555E'),
('PRI_F', '66666666F'),
('PRI_M', '66666666F'),
('SEG_M', '77777777G'),
('CAD_F', '88888888H'),
('CAD_M', '88888888H'),
('DH_F', '88888888H'),
('DH_M', '99999999I'),
('IF_F', '99999999I');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `id_cuotas` int(11) NOT NULL,
  `concepto` varchar(200) DEFAULT NULL,
  `metodo_pago` varchar(50) DEFAULT NULL,
  `importe` int(11) DEFAULT NULL,
  `dni` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`id_cuotas`, `concepto`, `metodo_pago`, `importe`, `dni`) VALUES
(1, 'Pago Manuel', 'PayPal', 200, '01234567C'),
(2, 'Pago cristina', 'PayPal', 400, '01234567C'),
(3, 'Pago Laro', 'PayPal', 320, '01234567C'),
(4, 'Pago Jorge', 'PayPal', 320, '01234567E'),
(5, 'Pago samuel', 'PayPal', 120, '01234567C'),
(6, 'Pago Marta', 'PayPal', 200, '23456789C'),
(7, 'Pago Natalia', 'PayPal', 140, '23456789L'),
(8, 'Pago Fernando ', 'PayPal', 320, '23456789S'),
(9, 'Pago Tonny', 'PayPal', 500, '01234567C'),
(10, 'Pago samuel', 'PayPal', 400, '23456789S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directivos`
--

CREATE TABLE `directivos` (
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `f_nacimiento` date DEFAULT NULL,
  `telefono` varchar(13) DEFAULT NULL,
  `cargo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `directivos`
--

INSERT INTO `directivos` (`dni`, `nombre`, `apellidos`, `f_nacimiento`, `telefono`, `cargo`) VALUES
('12345678A', 'Carlos', 'González Pérez', '1985-05-10', '+34 123456789', 'Presidente'),
('23456789B', 'María', 'López Rodríguez', '1990-09-20', '+34 234567890', 'Vicepresidenta'),
('34567890C', 'Javier', 'Martínez García', '1982-02-15', '+34 345678901', 'Director Deportivo'),
('45678901D', 'Ana', 'Sánchez Rodríguez', '1978-12-03', '+34 456789012', 'Secretaria'),
('56789012E', 'David', 'Fernández García', '1995-06-30', '+34 567890123', 'Tesorero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenadores`
--

CREATE TABLE `entrenadores` (
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `f_nacimiento` date DEFAULT NULL,
  `telefono` varchar(13) DEFAULT NULL,
  `sueldo` int(11) DEFAULT NULL,
  `cod_categoria` varchar(5) DEFAULT NULL,
  `dni_directivos` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entrenadores`
--

INSERT INTO `entrenadores` (`dni`, `nombre`, `apellidos`, `f_nacimiento`, `telefono`, `sueldo`, `cod_categoria`, `dni_directivos`) VALUES
('101010101', 'Mario', 'Vázquez', '1992-10-10', '+34669900112', 1500, 'SEG_M', '12345678A'),
('11111111A', 'Juan', 'García', '1990-01-01', '+34666555666', 2000, 'AL_F', '12345678A'),
('11111111J', 'Lucía', 'Ruiz', '1993-10-10', '+34665566778', 1900, 'IF_M', '56789012E'),
('11111111R', 'Alba', 'Vega', '1998-06-18', '+34661122334', 1500, 'SEG_M', '34567890C'),
('11111112A', 'Isabel', 'García', '1990-01-01', '+34660011223', 1800, 'IF_F', '23456789B'),
('22222222B', 'María', 'Sánchez', '1985-02-02', '+34667788990', 1800, 'AL_M', '23456789B'),
('22222222K', 'Pablo', 'Hernández', '1996-11-11', '+34667788990', 1800, 'JUV_F', '12345678A'),
('22222222S', 'Gonzalo', 'Blanco', '1993-07-19', '+34663344556', 1700, 'AL_F', '45678901D'),
('22222223B', 'David', 'Hernández', '1985-02-02', '+34662233445', 1900, 'IF_M', '34567890C'),
('33333333C', 'Pedro', 'Rodríguez', '1995-03-03', '+34661122334', 1500, 'BN_F', '34567890C'),
('33333333L', 'Laura', 'Díaz', '1990-12-12', '+34669900112', 1600, 'JUV_M', '23456789B'),
('33333333T', 'Carla', 'Luna', '1988-08-20', '+34665566778', 1900, 'AL_M', '56789012E'),
('33333334C', 'Carmen', 'González', '1993-03-03', '+34664455667', 1700, 'JUV_F', '45678901D'),
('44444444D', 'Ana', 'Gómez', '1992-04-04', '+34663344556', 1700, 'BN_M', '45678901D'),
('44444444M', 'David', 'Álvarez', '1989-01-13', '+34661122334', 1500, 'PB_F', '34567890C'),
('44444444U', 'Andrés', 'Soto', '1992-09-21', '+34667788990', 1800, 'BN_F', '12345678A'),
('44444445D', 'Miguel', 'Sánchez', '1987-04-04', '+34666677889', 1600, 'JUV_M', '56789012E'),
('55555555E', 'Luis', 'Fernández', '1988-05-05', '+34665566778', 1900, 'CAD_F', '56789012E'),
('55555555N', 'Elena', 'Gutiérrez', '1998-02-14', '+34663344556', 1700, 'PB_M', '45678901D'),
('55555555V', 'Mónica', 'Castillo', '1985-10-22', '+34669900112', 1600, 'BN_M', '23456789B'),
('55555556E', 'Sara', 'Gómez', '1999-05-05', '+34668899001', 1500, 'PB_F', '12345678A'),
('66666666F', 'Marta', 'Jiménez', '1987-06-06', '+34667788990', 1800, 'CAD_M', '12345678A'),
('66666666W', 'Javier', 'Núñez', '1987-11-23', '+34661122334', 1500, 'CAD_F', '34567890C'),
('66666667F', 'Pablo', 'Fernández', '1991-06-06', '+34661122334', 1600, 'PB_M', '23456789B'),
('77777777G', 'Carlos', 'González', '1991-07-07', '+34669900112', 1600, 'DH_F', '23456789B'),
('77777777O', 'Fernando', 'Martínez', '1999-03-15', '+34665566778', 1900, 'PRI_F', '56789012E'),
('77777777X', 'Irene', 'Ortega', '1994-12-24', '+34663344556', 1700, 'CAD_M', '45678901D'),
('77777778G', 'Laura', 'Pérez', '1988-07-07', '+34663344556', 1700, 'PRI_F', '34567890C'),
('88888888H', 'Sara', 'López', '1994-08-08', '+34661122334', 1500, 'DH_M', '34567890C'),
('88888888P', 'Clara', 'Torres', '1986-04-16', '+34667788990', 1800, 'PRI_M', '12345678A'),
('88888889H', 'Juan', 'Romero', '1995-08-08', '+34665566778', 1800, 'PRI_M', '45678901D'),
('99999990I', 'Lucía', 'Ramírez', '1986-09-09', '+34667788990', 1900, 'SEG_F', '56789012E'),
('99999999I', 'Jorge', 'Pérez', '1997-09-09', '+34663344556', 1700, 'IF_F', '45678901D'),
('99999999Q', 'Diego', 'Santos', '1991-05-17', '+34669900112', 1600, 'SEG_F', '23456789B');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarioentrenamientos`
--

CREATE TABLE `horarioentrenamientos` (
  `id` int(11) NOT NULL,
  `diasemana` varchar(20) DEFAULT NULL,
  `hinicio` time DEFAULT NULL,
  `hfinal` time DEFAULT NULL,
  `cod_categoria` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horarioentrenamientos`
--

INSERT INTO `horarioentrenamientos` (`id`, `diasemana`, `hinicio`, `hfinal`, `cod_categoria`) VALUES
(1, '0', '17:30:00', '19:00:00', 'BN_M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juegan`
--

CREATE TABLE `juegan` (
  `id_juegan` int(11) NOT NULL,
  `dni` varchar(9) DEFAULT NULL,
  `id_partidos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `juegan`
--

INSERT INTO `juegan` (`id_juegan`, `dni`, `id_partidos`) VALUES
(26, NULL, 2),
(1, '01234567C', 1),
(11, '01234567C', 3),
(2, '01234567E', 1),
(3, '01234567J', 1),
(4, '01234567L', 1),
(5, '01234567T', 1),
(6, '12345678B', 1),
(7, '12345678M', 1),
(8, '23456789L', 1),
(9, '25836914F', 1),
(10, '25836945J', 1),
(20, '34567890X', 2),
(21, '45678901X', 2),
(22, '45678901Y', 2),
(23, '56789012E', 2),
(24, '56789012H', 2),
(12, '56789012O', 1),
(16, '56789012O', 5),
(25, '67890123F', 2),
(13, '67890123P', 1),
(17, '67890123P', 5),
(14, '78901234H', 1),
(18, '78901234H', 5),
(15, '89012345J', 1),
(19, '89012345J', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugadores`
--

CREATE TABLE `jugadores` (
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `f_nacimiento` date DEFAULT NULL,
  `telefono` varchar(13) DEFAULT NULL,
  `cod_categoria` varchar(5) DEFAULT NULL,
  `dorsal` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `jugadores`
--

INSERT INTO `jugadores` (`dni`, `nombre`, `apellidos`, `f_nacimiento`, `telefono`, `cod_categoria`, `dorsal`) VALUES
('01234567C', 'Cristina', 'Fernández García', '2005-10-07', '+34333765432', 'IF_F', 0),
('01234567E', 'Patricia', 'Sánchez López', '1989-04-23', '+34333123456', 'IF_F', 0),
('01234567J', 'Luis', 'Sánchez', '2003-07-01', '+34123456798', 'CAD_F', 0),
('01234567L', 'Miguel', 'Sánchez López', '1989-01-23', '+34333765432', 'IF_M', 0),
('01234567T', 'Álvaro', 'Hernández', '2001-11-11', '+34123456808', 'JUV_M', 0),
('04587241F', 'Elena', 'Fernández Rodríguez', '2004-05-23', '+34 645239876', 'AL_F', 0),
('12345678A', 'Juan', 'García', '2003-03-08', '+34123456789', 'AL_F', 0),
('12345678B', 'Javier', 'García López', '1998-05-14', '+34333567890', 'AL_M', 0),
('12345678C', 'Cristina', 'Núñez Sánchez', '2003-08-17', '+34 610348975', 'AL_F', 0),
('12345678F', 'Cristina', 'Gómez Martín', '2003-11-01', '+34333765432', 'JUV_F', 0),
('12345678K', 'Alba', 'Gutierrez', '2000-10-28', '+34123456799', 'CAD_M', 0),
('12345678M', 'Rubén', 'Gómez Martín', '2003-10-11', '+34333123456', 'JUV_M', 0),
('12345678V', 'Ana', 'García López', '1998-03-14', '+34333123456', 'AL_F', 0),
('12345679B', 'Ana', 'Sánchez López', '2006-05-23', '+34333234567', 'IF_F', 0),
('19876543A', 'Paula', 'López Pérez', '2002-11-14', '+34 691345678', 'AL_F', 0),
('23456789A', 'Lucía', 'Gómez Martín', '2007-12-01', '+34333678901', 'JUV_F', 0),
('23456789B', 'Pedro', 'López', '2002-05-15', '+34123456790', 'AL_F', 0),
('23456789C', 'Pablo', 'Fernández Martínez', '1997-02-25', '+34333345678', 'AL_M', 0),
('23456789G', 'Isabel', 'Fernández Sánchez', '2004-08-12', '+34333234567', 'JUV_F', 0),
('23456789K', 'María', 'Fernández García', '1997-03-14', '+34333456789', 'AL_F', 0),
('23456789L', 'Mario', 'Rodríguez', '2002-01-17', '+34123456800', 'CAD_M', 0),
('23456789N', 'Adrián', 'Fernández Sánchez', '2004-07-22', '+34333678901', 'JUV_M', 0),
('23456789S', 'Sara', 'Hernández García', '2004-10-03', '+34 634567890', 'AL_F', 0),
('23456789W', 'Sara', 'Fernández Martínez', '1997-05-25', '+34333678901', 'AL_F', 0),
('23878716B', 'Ana', 'Martínez Pérez', '2003-09-15', '+34 619876541', 'AL_F', 0),
('25836914F', 'Mario', 'Sánchez García', '2000-09-05', '+34 699876543', 'AL_M', 0),
('25836945J', 'Carlos', 'Jiménez Pérez', '2000-02-01', '+34 622345091', 'AL_M', 0),
('29876654D', 'Javier', 'González Ruiz', '2001-05-10', '+34 622345678', 'AL_M', 0),
('34567890B', 'Carla', 'Fernández Sánchez', '2008-09-12', '+34333123456', 'JUV_F', 0),
('34567890C', 'María', 'González', '2001-09-20', '+34123456791', 'AL_M', 0),
('34567890D', 'Hugo', 'Gómez Fernández', '1996-09-17', '+34333234567', 'BN_M', 0),
('34567890H', 'Álex', 'Martínez González', '2005-05-23', '+34333901234', 'PB_M', 0),
('34567890J', 'Laura', 'Gómez Martín', '1998-05-25', '+34333123456', 'AL_F', 0),
('34567890M', 'Sara', 'López', '2002-08-05', '+34123456801', 'DH_F', 0),
('34567890O', 'Sofía', 'Martínez González', '2005-04-13', '+34333456789', 'PB_F', 0),
('34567890X', 'Lucía', 'Gómez Fernández', '1996-07-17', '+34333456789', 'BN_F', 0),
('34567891B', 'Laura', 'Martínez García', '2000-01-30', '+34 600987654', 'AL_F', 0),
('35971246H', 'Alejandro', 'Gómez Hernández', '2003-11-18', '+34 611432198', 'AL_M', 0),
('45678901C', 'Marcos', 'Martínez González', '2013-06-23', '+34333123456', 'PB_M', 0),
('45678901D', 'Lucía', 'Martínez', '2000-11-10', '+34123456792', 'AL_M', 0),
('45678901E', 'Marcos', 'Sánchez Martín', '1995-12-04', '+34333765432', 'BN_M', 0),
('45678901I', 'Héctor', 'Sánchez López', '2006-03-11', '+34333678901', 'PB_M', 0),
('45678901N', 'Manuel', 'García', '2001-06-09', '+34123456802', 'DH_F', 0),
('45678901P', 'Ainhoa', 'Sánchez López', '2006-01-01', '+34333123456', 'PB_F', 0),
('45678901X', 'Marta', 'González Fernández', '1999-07-17', '+34333901234', 'BN_F', 0),
('45678901Y', 'Marta', 'Sánchez Martín', '1995-08-04', '+34333123456', 'BN_F', 0),
('45678912T', 'Isabel', 'Jiménez Sánchez', '2001-06-25', '+34 654890321', 'AL_F', 0),
('46983214E', 'Daniel', 'Pérez López', '2002-02-20', '+34 665432189', 'AL_M', 0),
('53789653W', 'Lucía', 'García González', '2002-03-12', '+34 689345120', 'AL_F', 0),
('56789012D', 'Alejandro', 'Sánchez López', '2014-04-11', '+34333678901', 'PB_M', 0),
('56789012E', 'Javier', 'Fernández', '2001-12-25', '+34123456793', 'BN_F', 0),
('56789012F', 'Pedro', 'Martínez González', '1994-11-22', '+34333890123', 'CAD_M', 0),
('56789012H', 'Paula', 'Sánchez López', '2000-09-04', '+34333678901', 'BN_F', 0),
('56789012J', 'Lidia', 'Jiménez García', '2009-10-01', '+34333765432', 'PRI_F', 0),
('56789012O', 'Natalia', 'Martínez', '2000-04-19', '+34123456803', 'DH_M', 0),
('56789012Q', 'Álvaro', 'García Fernández', '2007-02-28', '+34333901234', 'PB_M', 0),
('56789012Z', 'María', 'Martínez González', '1994-01-22', '+34333901234', 'CAD_F', 0),
('60523417P', 'María', 'Sánchez López', '2001-12-01', '+34 677984565', 'AL_F', 0),
('67890023P', 'Sara', 'Martínez González', '2001-02-22', '+34333349876', 'CAD_F', 0),
('67890123A', 'Carmen', 'López Sánchez', '1993-02-05', '+34333678901', 'CAD_F', 0),
('67890123E', 'Nerea', 'Jiménez García', '2017-11-01', '+34333901234', 'PRI_F', 0),
('67890123F', 'Sofía', 'Pérez', '2000-01-07', '+34123456794', 'BN_F', 0),
('67890123G', 'Mario', 'López Sánchez', '1993-06-30', '+34333349876', 'CAD_M', 0),
('67890123K', 'Elena', 'González Fernández', '2010-01-02', '+34333349876', 'PRI_F', 0),
('67890123P', 'Daniel', 'Sánchez', '2003-02-27', '+34123456804', 'DH_M', 0),
('67890123R', 'Iker', 'López Martín', '2008-05-17', '+34333678901', 'PB_M', 0),
('67893456K', 'Julia', 'Gómez Ruiz', '2000-08-07', '+34 611345789', 'AL_F', 0),
('68257931K', 'Manuel', 'López Ruiz', '2002-10-15', '+34 665432190', 'AL_M', 0),
('76849312G', 'Adrián', 'Fernández González', '2004-04-02', '+34 677983256', 'AL_M', 0),
('78901234B', 'Lola', 'Díaz Jiménez', '1992-06-17', '+34333123456', 'DH_F', 0),
('78901234F', 'Irene', 'González Fernández', '2018-02-02', '+34333456789', 'PRI_F', 0),
('78901234G', 'David', 'Gómez', '2000-06-14', '+34123456795', 'BN_M', 0),
('78901234H', 'David', 'Díaz Jiménez', '1992-03-15', '+34333567890', 'DH_M', 0),
('78901234L', 'Lucía', 'Díaz Jiménez', '2011-04-03', '+34333456789', 'SEG_F', 0),
('78901234Q', 'Ana', 'Fernández', '2001-05-22', '+34123456805', 'JUV_F', 0),
('78901234S', 'Natalia', 'Jiménez García', '2009-08-09', '+34333765432', 'PRI_F', 0),
('78901254F', 'Lidia', 'López Sánchez', '2002-03-05', '+34333901234', 'CAD_F', 0),
('89012345C', 'Carla', 'González Fernández', '1991-11-29', '+34333456789', 'DH_F', 0),
('89012345E', 'Natalia', 'Díaz Jiménez', '2003-07-17', '+34333123456', 'DH_F', 0),
('89012345H', 'Elena', 'Ruiz', '2001-08-03', '+34123456796', 'BN_M', 0),
('89012345J', 'Alejandro', 'González Fernández', '1991-08-29', '+34333876543', 'DH_M', 0),
('89012345M', 'Sofía', 'González Fernández', '2012-07-04', '+34333123456', 'SEG_F', 0),
('89012345R', 'Rubén', 'Gómez', '2000-09-16', '+34123456806', 'JUV_F', 0),
('89012345T', 'Valeria', 'González Fernández', '2010-11-02', '+34333349876', 'PRI_F', 0),
('89012345X', 'Laura', 'Díaz Jiménez', '2019-05-03', '+34333123456', 'SEG_F', 0),
('90123056D', 'Alicia', 'González Fernández', '2004-12-29', '+34333456789', 'DH_F', 0),
('90123456D', 'Silvia', 'Fernández García', '1990-09-07', '+34333678901', 'IF_F', 0),
('90123456H', 'Celia', 'González Fernández', '2020-08-04', '+34333901234', 'SEG_F', 0),
('90123456I', 'Carla', 'Hernández', '2002-04-12', '+34123456797', 'CAD_F', 0),
('90123456K', 'Jorge', 'Fernández García', '1990-04-07', '+34333234567', 'IF_M', 0),
('90123456S', 'Carmen', 'Ruiz', '2000-03-18', '+34123456807', 'JUV_M', 0),
('95135786I', 'Jorge', 'Hernández Sánchez', '2001-07-08', '+34 677889012', 'AL_M', 0),
('98765432L', 'Miguel', 'Martínez García', '2001-01-12', '+34 699876543', 'AL_M', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidos`
--

CREATE TABLE `partidos` (
  `id_partidos` int(11) NOT NULL,
  `resultadoS` int(11) DEFAULT NULL,
  `resultadoC` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `competicion` varchar(50) DEFAULT NULL,
  `contrincante` varchar(50) DEFAULT NULL,
  `cod_categoria` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `partidos`
--

INSERT INTO `partidos` (`id_partidos`, `resultadoS`, `resultadoC`, `fecha`, `hora`, `competicion`, `contrincante`, `cod_categoria`) VALUES
(1, NULL, NULL, '2023-03-25', '19:00:00', 'Liga MGS', 'Tenis ', 'DH_M'),
(2, NULL, NULL, '2023-03-12', '15:00:00', 'Liga benjamín femenino', 'Jolaseta', 'BN_F'),
(3, NULL, NULL, '2023-03-05', '10:15:00', 'LIga MGS', 'FC Barcelona', 'DH_M'),
(4, NULL, NULL, '2023-03-11', '10:05:00', 'Liga Norte', 'San Sebastian', 'PRI_M'),
(5, NULL, NULL, '2023-05-17', '13:00:00', 'Liga MGS', 'Tenis ', 'DH_M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patrocinadores`
--

CREATE TABLE `patrocinadores` (
  `id_patrocinadores` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `dinero` float(10,5) DEFAULT NULL,
  `dni` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `patrocinadores`
--

INSERT INTO `patrocinadores` (`id_patrocinadores`, `nombre`, `dinero`, `dni`) VALUES
(1, 'Nike', 5000.00000, '12345678A'),
(2, 'Adidas', 2500.00000, '23456789B'),
(3, 'Under Armour', 10000.00000, '34567890C'),
(4, 'Puma', 7500.00000, '45678901D'),
(5, 'Reebok', 3000.00000, '56789012E'),
(6, 'ASICS', 2000.00000, '12345678A'),
(7, 'New Balance', 4000.00000, '23456789B'),
(8, 'Wilson', 3000.00000, '34567890C'),
(9, 'Rawlings', 5000.00000, '45678901D'),
(10, 'Mizuno', 6000.00000, '56789012E'),
(11, 'Head', 4500.00000, '12345678A'),
(12, 'Yonex', 3500.00000, '23456789B'),
(13, 'Babolat', 5500.00000, '34567890C'),
(14, 'Prince', 2500.00000, '45678901D'),
(15, 'Easton', 1500.00000, '56789012E');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preparadores`
--

CREATE TABLE `preparadores` (
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `f_nacimiento` date DEFAULT NULL,
  `telefono` varchar(13) DEFAULT NULL,
  `sueldo` int(11) DEFAULT NULL,
  `dni_directivos` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `preparadores`
--

INSERT INTO `preparadores` (`dni`, `nombre`, `apellidos`, `f_nacimiento`, `telefono`, `sueldo`, `dni_directivos`) VALUES
('11111111A', 'Juan', 'García Pérez', '1992-07-01', '+34 111111111', 150, '12345678A'),
('12121212J', 'María', 'López García', '1994-02-16', '+34 121212121', 190, '56789012E'),
('13131313K', 'Juan', 'Martínez Pérez', '1986-04-30', '+34 131313131', 170, '12345678A'),
('14141414L', 'Laura', 'Gómez López', '1999-11-05', '+34 141414141', 160, '12345678A'),
('15151515M', 'Pedro', 'Hernández Ruiz', '1992-09-28', '+34 151515151', 200, '23456789B'),
('16161616N', 'María', 'Sánchez Martínez', '1987-01-12', '+34 161616161', 180, '23456789B'),
('22222222B', 'Laura', 'Fernández López', '1988-10-22', '+34 222222222', 170, '12345678A'),
('33333333C', 'Pedro', 'Ruiz García', '1990-02-05', '+34 333333333', 200, '23456789B'),
('44444444D', 'María', 'Gómez Martínez', '1985-12-11', '+34 444444444', 180, '23456789B'),
('55555555E', 'Miguel', 'Hernández Sánchez', '1993-05-18', '+34 555555555', 100, '34567890C'),
('66666666F', 'Lucía', 'Martín González', '1998-03-24', '+34 666666666', 190, '34567890C'),
('77777777G', 'Antonio', 'Sánchez López', '1987-09-15', '+34 777777777', 220, '45678901D'),
('88888888H', 'Ana', 'García Martínez', '1995-06-12', '+34 888888888', 200, '45678901D'),
('99999999I', 'Javier', 'Fernández Pérez', '1991-08-08', '+34 999999999', 180, '56789012E');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id_telefonos` int(11) NOT NULL,
  `dni` varchar(9) DEFAULT NULL,
  `telefonos_tutores_legales` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id_telefonos`, `dni`, `telefonos_tutores_legales`) VALUES
(2, '12345678A', '+34601234567'),
(22, '34567890C', '+34601234567'),
(12, '34567891B', '+34601234567'),
(3, '12345678C', '+34612345678'),
(23, '35971246H', '+34612345678'),
(13, '45678912T', '+34612345678'),
(4, '12345678V', '+34623456789'),
(24, '45678901D', '+34623456789'),
(14, '53789653W', '+34623456789'),
(5, '19876543A', '+34634567890'),
(25, '46983214E', '+34634567890'),
(15, '60523417P', '+34634567890'),
(6, '23456789B', '+34645678901'),
(16, '67893456K', '+34645678901'),
(17, '12345678B', '+34656789012'),
(7, '23456789K', '+34656789012'),
(18, '23456789C', '+34667890123'),
(8, '23456789S', '+34667890123'),
(1, '04587241F', '+34678901234'),
(9, '23456789W', '+34678901234'),
(19, '25836914F', '+34678901234'),
(10, '23878716B', '+34689012345'),
(20, '25836945J', '+34689012345'),
(21, '29876654D', '+34690123456'),
(11, '34567890J', '+34690123456');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`cod_categoria`),
  ADD KEY `fk_categoria_preparadores` (`dni`);

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`id_cuotas`),
  ADD KEY `fk_cuotas_jugadores` (`dni`);

--
-- Indices de la tabla `directivos`
--
ALTER TABLE `directivos`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `entrenadores`
--
ALTER TABLE `entrenadores`
  ADD PRIMARY KEY (`dni`),
  ADD KEY `fk_entrenadores_categoria` (`cod_categoria`),
  ADD KEY `fk_entrenadores_directivos` (`dni_directivos`);

--
-- Indices de la tabla `horarioentrenamientos`
--
ALTER TABLE `horarioentrenamientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_horarioentrenamientos_categoria` (`cod_categoria`);

--
-- Indices de la tabla `juegan`
--
ALTER TABLE `juegan`
  ADD PRIMARY KEY (`id_juegan`),
  ADD UNIQUE KEY `uk_jugadores_partidos` (`dni`,`id_partidos`),
  ADD KEY `fk_juegan_partidos` (`id_partidos`);

--
-- Indices de la tabla `jugadores`
--
ALTER TABLE `jugadores`
  ADD PRIMARY KEY (`dni`),
  ADD KEY `fk_jugadores_categoria` (`cod_categoria`);

--
-- Indices de la tabla `partidos`
--
ALTER TABLE `partidos`
  ADD PRIMARY KEY (`id_partidos`),
  ADD KEY `fk_partidos_categoria` (`cod_categoria`);

--
-- Indices de la tabla `patrocinadores`
--
ALTER TABLE `patrocinadores`
  ADD PRIMARY KEY (`id_patrocinadores`),
  ADD KEY `fk_directivos_patrocinadores` (`dni`);

--
-- Indices de la tabla `preparadores`
--
ALTER TABLE `preparadores`
  ADD PRIMARY KEY (`dni`),
  ADD KEY `fk_preparadores_directivos` (`dni_directivos`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id_telefonos`),
  ADD UNIQUE KEY `uk_telefonos_jugadores` (`telefonos_tutores_legales`,`dni`),
  ADD KEY `fk_telefonos_jugadores` (`dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `id_cuotas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `horarioentrenamientos`
--
ALTER TABLE `horarioentrenamientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `juegan`
--
ALTER TABLE `juegan`
  MODIFY `id_juegan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `partidos`
--
ALTER TABLE `partidos`
  MODIFY `id_partidos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `patrocinadores`
--
ALTER TABLE `patrocinadores`
  MODIFY `id_patrocinadores` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id_telefonos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `fk_categoria_preparadores` FOREIGN KEY (`dni`) REFERENCES `preparadores` (`dni`);

--
-- Filtros para la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD CONSTRAINT `fk_cuotas_jugadores` FOREIGN KEY (`dni`) REFERENCES `jugadores` (`dni`);

--
-- Filtros para la tabla `entrenadores`
--
ALTER TABLE `entrenadores`
  ADD CONSTRAINT `fk_entrenadores_categoria` FOREIGN KEY (`cod_categoria`) REFERENCES `categoria` (`cod_categoria`),
  ADD CONSTRAINT `fk_entrenadores_directivos` FOREIGN KEY (`dni_directivos`) REFERENCES `directivos` (`dni`);

--
-- Filtros para la tabla `horarioentrenamientos`
--
ALTER TABLE `horarioentrenamientos`
  ADD CONSTRAINT `fk_horarioentrenamientos_categoria` FOREIGN KEY (`cod_categoria`) REFERENCES `categoria` (`cod_categoria`);

--
-- Filtros para la tabla `juegan`
--
ALTER TABLE `juegan`
  ADD CONSTRAINT `fk_juegan_jugadores` FOREIGN KEY (`dni`) REFERENCES `jugadores` (`dni`),
  ADD CONSTRAINT `fk_juegan_partidos` FOREIGN KEY (`id_partidos`) REFERENCES `partidos` (`id_partidos`);

--
-- Filtros para la tabla `jugadores`
--
ALTER TABLE `jugadores`
  ADD CONSTRAINT `fk_jugadores_categoria` FOREIGN KEY (`cod_categoria`) REFERENCES `categoria` (`cod_categoria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
